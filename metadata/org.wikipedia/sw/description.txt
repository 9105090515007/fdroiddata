Tuma maoni yako kuhusu apu! Kwa menu,bonyeza "Settings", kisha" About the wikipedia app",kisha "Send app feedback".

Sera ya faragha: https://m.wikimediafoundation.org/wiki/Privacy_policy

Masharti za kutumika: https://m.wikimediafoundation.org/wiki/Terms_of_Use

Kuhusu Msingi wa Wikimedia

Msingi wa Wikimedia ni shirika lisilo la faida linalo fadhili Wikipedia na miradi mingine ya Wikimedia. Msingi huu ni wa hisani na hufadhiliwa hasa kwa michango. Kwa maelezo zaidi, zuru tovuti yetu:https://wikimediafoundation.org/wiki/Home.
